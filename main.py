import tidalapi
from csv import CSVFile
import datetime


def today_as_string():
    return datetime.date.today().strftime("%B %d %Y")

# provide user with login link for Tidal
session = tidalapi.Session()
login, future = session.login_oauth()
print("http://" + login.verification_uri_complete)
future.result()

user = session.user

# output songs
songs = CSVFile("favorite tidal songs - " + today_as_string() + ".csv")
songs.set_headers(["Title", "Artist"])
for song in user.favorites.tracks():
    songs.add_row([song.name, song.artist.name])
songs.write()

# output albums
albums = CSVFile("favorite tidal albums - " + today_as_string() + ".csv")
albums.set_headers(["Title", "Artist"])
for album in user.favorites.albums():
    albums.add_row([album.name, album.artist.name])
albums.write()

# output artists
artists = CSVFile("favorite tidal artists - " + today_as_string() + ".csv")
artists.set_headers(["Name"])
for artist in user.favorites.artists():
    artists.add_row([artist.name])
artists.write()

# output playlists
for playlist in user.playlists():
    playlist_csv = CSVFile("[tidal playlist] - " + playlist.name + " - " + today_as_string() + ".csv")
    playlist_csv.set_headers(["Artist", "Track Title", "Album Title"])
    pl_obj = session.get_playlist_tracks(playlist.id)
    for track in pl_obj:
        playlist_csv.add_row([track.artist.name, track.name, track.album.name])
    playlist_csv.write()
