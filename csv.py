class CSVFile:
    def __init__(self, filename):
        self.filename = filename
        self.headers = None
        self.rows = []

    def set_headers(self, headers):
        self.headers = headers

    def add_row(self, row):
        self.rows.append(row)

    def __build_csv(self):
        # append header
        txt = "\"" + "\", \"".join(self.headers) + "\""
        # append rows
        for row in self.rows:
            txt += "\n\"" + "\", \"".join(row) + "\""
        return txt

    def write(self):
        content = self.__build_csv()
        f = open(self.filename, "w")
        f.write(content)
        f.close()
