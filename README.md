# py-tidal  
 
py-tidal is a simple script for saving the following information from your Tidal music account locally in CSV files:

- Albums
- Artists
- Songs
- Playlists

## Why py-tidal?

Originally I intended this to be a collection of scripts for managing my Tidal library.  However, I've recently decided to cancel my Tidal subscription, so the objective has become much simpler: get a copy of my favorites and playlists from Tidal so that I can import them into my new platform of choice (still deliberating that :-]).

## License
 
The MIT License (MIT)

Copyright (c) 2021 Rom�n M. Pacheco

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.